package com.sisbam.sgp.entity.administration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(name = "estadoProyecto", catalog = "sgr")
public class EstadoProyecto implements java.io.Serializable{

	private int idEstado;
	private String nombreEstado;
	
	
	public EstadoProyecto(){
		
	}
	
	public EstadoProyecto(int idEstado, String nombreEstado) {
		super();
		this.idEstado = idEstado;
		this.nombreEstado = nombreEstado;
	}

	@Id
	@GeneratedValue
	@Column(name="idEstado")
	public int getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}

	@Column(name= "nombreEstado", nullable= false, length=20)
	public String getNombreEstado() {
		return nombreEstado;
	}

	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}
	
	

	

}
