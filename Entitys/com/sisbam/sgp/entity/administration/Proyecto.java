package com.sisbam.sgp.entity.administration;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.type.DateType;



@Entity
@Table(name = "proyecto", catalog = "sgr")
public class Proyecto implements java.io.Serializable{

	private int codProyecto;
	private String nombreProyecto;
	private DateType fechaInicio;
	private DateType fechaFin;
	private String correoDelPropietario;
	
	private EstadoProyecto estadoProyecto;
	private TipoProyecto tipoProyecto;

	public Proyecto(int codProyecto, String nombreProyecto, DateType fechaInicio, DateType fechaFin,
			String correoDelPropietario, EstadoProyecto estadoProyecto, TipoProyecto tipoProyecto) {
		this.codProyecto = codProyecto;
		this.nombreProyecto = nombreProyecto;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.correoDelPropietario = correoDelPropietario;
		this.estadoProyecto = estadoProyecto;
		this.tipoProyecto = tipoProyecto;
	}
	
	
	public Proyecto() {
		
	}

	@Id
	@GeneratedValue
	@Column(name="codProyecto")
	public int getCodProyecto() {
		return codProyecto;
	}

	public void setCodProyecto(int codProyecto) {
		this.codProyecto = codProyecto;
	}

	@Column(name= "nombreProyecto", nullable= false, length=50)
	public String getNombreProyecto() {
		return nombreProyecto;
	}

	public void setNombreProyecto(String nombreProyecto) {
		this.nombreProyecto = nombreProyecto;
	}

	public DateType getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(DateType fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public DateType getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(DateType fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Column(name= "correoDelPropietario", nullable= false, length=20)
	public String getCorreoDelPropietario() {
		return correoDelPropietario;
	}

	public void setCorreoDelPropietario(String correoDelPropietario) {
		this.correoDelPropietario = correoDelPropietario;
	}

	@OneToOne(cascade= CascadeType.ALL)
	public EstadoProyecto getEstadoProyecto() {
		return estadoProyecto;
	}
	
	public void setEstadoProyecto(EstadoProyecto estadoProyecto) {
		this.estadoProyecto = estadoProyecto;
	}
	
	@OneToOne(cascade= CascadeType.ALL)
	public TipoProyecto getTipoProyecto() {
		return tipoProyecto;
	}

	public void setTipoProyecto(TipoProyecto tipoProyecto) {
		this.tipoProyecto = tipoProyecto;
	}	

}
