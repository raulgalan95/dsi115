package com.sisbam.sgp.entity.administration;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "especificacionProyecto", catalog = "sgr")
public class EspecificacionProyecto implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private int idEspecificacion;
	private String objetivoEspecifico;
	private FormularioProyecto especificacionFormulario;

	public EspecificacionProyecto() {

	}

	public EspecificacionProyecto(int idEspecificacion, FormularioProyecto especificacionFormulario,
			FormularioProyecto formularioProyecto) {
		this.idEspecificacion = idEspecificacion;
		this.objetivoEspecifico = objetivoEspecifico;
		this.especificacionFormulario = especificacionFormulario;

	}

	@Id
	@GeneratedValue
	@Column(name = "idEspecificacion")
	public int getIdEspecificacion() {
		return idEspecificacion;
	}

	public void setIdEspecificacion(int idEspecificacion) {
		this.idEspecificacion = idEspecificacion;
	}

	@Column(name = "objetivoEspecifico", length = 50)
	public String getObjetivoEspecifico() {
		return objetivoEspecifico;
	}

	public void setObjetivoEspecifico(String objetivoEspecifico) {
		this.objetivoEspecifico = objetivoEspecifico;
	}

	@ManyToOne(cascade= CascadeType.ALL)
	public FormularioProyecto getEspecificacionFormulario() {
		return especificacionFormulario;
	}

	public void setEspecificacionFormulario(FormularioProyecto especificacionFormulario) {
		this.especificacionFormulario = especificacionFormulario;
	}

	


}