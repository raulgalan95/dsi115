package com.sisbam.sgp.entity.administration;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "formularioProyecto", catalog = "sgr")
public class FormularioProyecto implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private int idFormulario;
	private Proyecto proyecto;
	private String justificacion;
	private String lineaBase;
	private String estrategiaImplementacion;
	private String estrategiaInstitucionalizacion;
	private String objetivoGeneral;
	private String planteamientoSocial;
	
	public FormularioProyecto() {
		
	}
	
	public FormularioProyecto(Proyecto proyecto, String justificacion, String lineaBase,
			String estrategiaImplementacion, String estrategiaInstitucionalizacion, String objetivoGeneral,
			String planteamientoSocial) {

		this.proyecto = proyecto;
		this.justificacion = justificacion;
		this.lineaBase = lineaBase;
		this.estrategiaImplementacion = estrategiaImplementacion;
		this.estrategiaInstitucionalizacion = estrategiaInstitucionalizacion;
		this.objetivoGeneral = objetivoGeneral;
		this.planteamientoSocial = planteamientoSocial;
	}
	
	@Id
	@GeneratedValue
	@Column(name="IdFormulario")
	public int getIdFormulario() {
		return idFormulario;
	}

	public void setIdFormulario(int idFormulario) {
		this.idFormulario = idFormulario;
	}

	@OneToOne(cascade= CascadeType.ALL)
	public Proyecto getProyecto() {
		return proyecto;
	}

	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	@Column(name= "justificacion", nullable= false, length=150)
	public String getJustificacion() {
		return justificacion;
	}

	public void setJustificacion(String justificacion) {
		this.justificacion = justificacion;
	}
	
	@Column(name= "lineaBase", nullable= false, length=150)
	public String getLineaBase() {
		return lineaBase;
	}

	public void setLineaBase(String lineaBase) {
		this.lineaBase = lineaBase;
	}

	@Column(name= "estrategiaImplementacion", nullable= false, length=150)
	public String getEstrategiaImplementacion() {
		return estrategiaImplementacion;
	}

	public void setEstrategiaImplementacion(String estrategiaImplementacion) {
		this.estrategiaImplementacion = estrategiaImplementacion;
	}

	@Column(name= "estrategiaInstitucionalizacion", length=150)
	public String getEstrategiaInstitucionalizacion() {
		return estrategiaInstitucionalizacion;
	}

	public void setEstrategiaInstitucionalizacion(String estrategiaInstitucionalizacion) {
		this.estrategiaInstitucionalizacion = estrategiaInstitucionalizacion;
	}

	@Column(name= "objetivoGeneral", length=50)
	public String getObjetivoGeneral() {
		return objetivoGeneral;
	}

	public void setObjetivoGeneral(String objetivoGeneral) {
		this.objetivoGeneral = objetivoGeneral;
	}

	@Column(name= "planteamientoSocial", length=150)
	public String getPlanteamientoSocial() {
		return planteamientoSocial;
	}

	public void setPlanteamientoSocial(String planteamientoSocial) {
		this.planteamientoSocial = planteamientoSocial;
	}
	
	
	
	
}

	
	
	

