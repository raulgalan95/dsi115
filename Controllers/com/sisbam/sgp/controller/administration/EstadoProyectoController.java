package com.sisbam.sgp.controller.administration;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.sisbam.sgp.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sgp.dao.DaoImp;
import com.sisbam.sgp.entity.administration.EspecificacionProyecto;
import com.sisbam.sgp.entity.administration.EstadoProyecto;
import com.sisbam.sgp.entity.security.Permisos;

@Controller
public class EstadoProyectoController {

	@Autowired
	private DaoImp manage_entity;

	private String path = "Administration/Proyecto/";
	private static final String IDENTIFICADOR = "estadoP";

	private Permisos permisos;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/estadoproyectos", method = RequestMethod.GET)

	// INDEX
	public String index(Model model, HttpServletRequest request) {

		String retorno = "403";

		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sgp/estadoproyectos", request, manage_entity, IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-" + IDENTIFICADOR);

		if (permisos.isR()) {
			EstadoProyecto estadoProyecto = new EstadoProyecto();
			model.addAttribute("estadoProyectoForm", estadoProyecto);
			model.addAttribute("estadoProyecto", null);
			List<EstadoProyecto> estadoproyectos = (List<EstadoProyecto>) this.manage_entity.getAll("EstadoProyecto");
			model.addAttribute("estadoproyectos", estadoproyectos);
			retorno = path + "estadoProyecto";
		}
		return retorno;
	}

	// GUARDAR
	@RequestMapping(value = "/estadoproyectos/add", method = RequestMethod.POST)
	public String saveOrUpadateEstadoProyecto(
			@ModelAttribute("estadoProyectoForm") EstadoProyecto estadoProyectoRecibido,
			Model model) throws ClassNotFoundException {
		String retorno = "403";
		if (permisos.isC()) {
			EstadoProyecto estadoProyecto = estadoProyectoRecibido;
			if (estadoProyecto.getIdEstado() == 0) {
				manage_entity.save(EstadoProyecto.class.getName(), estadoProyecto);
			} else {
				manage_entity.update(EstadoProyecto.class.getName(), estadoProyecto);
			}
			retorno = "redirect:/estadoproyectos";
		}
		return retorno;
	}

	// ACTUALIZAR
	@RequestMapping(value = "/estadoproyectos/update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") String estadoProyectoId, Model model, HttpServletRequest request)
			throws ClassNotFoundException {
		String retorno = "403";
		if (permisos.isU()) {
			EstadoProyecto estadoProyecto = (EstadoProyecto) manage_entity
					.getById(EstadoProyecto.class.getName(), Integer.parseInt(estadoProyectoId));
			model.addAttribute("estadoProyecto", estadoProyecto);
			EstadoProyecto estadoProyectoForm = new EstadoProyecto();
			model.addAttribute("estadoProyectoForm", estadoProyectoForm);
			retorno = path + "estadoProyecto-form";
		}

		return retorno;
	}

	// ELIMINAR
	@RequestMapping(value = "/estadoproyectos/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") String estadoProyectoId, Model model)
			throws ClassNotFoundException {
		String retorno = "403";
		if (permisos.isD()) {
			EstadoProyecto estadoProyecto = (EstadoProyecto) manage_entity
					.getById(EstadoProyecto.class.getName(), Integer.parseInt(estadoProyectoId));
			manage_entity.delete(EstadoProyecto.class.getName(), estadoProyecto);
			model.addAttribute("estadoProyecto", estadoProyecto);

			EstadoProyecto estadoProyectoForm = new EstadoProyecto();
			model.addAttribute("estadoProyectoForm", estadoProyectoForm);
			retorno = "redirect:/estadoproyectos";
		}
		return retorno;
	}

}
