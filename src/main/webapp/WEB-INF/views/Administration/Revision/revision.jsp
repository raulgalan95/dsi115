 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div id="contenido" class="card-panel hoverable">

		<hr>	
	 	<div class="container">	
<c:if test="${readtipoS}">	
			<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Nombre Proyecto</th>
						<th>Estado</th>
						<th>Responsable</th>
						<th>Justificacion</th>
						<th>Objeto</th>
						<th>Correo</th>
						<th>Estado</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${solicitudes}" var="solicitud">
						<tr style="color:#0B0B61;">
							<td>${solicitud.titulo } </td>
							<td>${solicitud.estado }</td>
							<td>${solicitud.usuario.username }</td>
							<td>${solicitud.justificacion }</td>
							<td>${solicitud.objeto }</td>
							<td>${solicitud.correo }</td>
							<td width="5%">
							<c:if test="${updatetipoS}">	
									<a class="modal-trigger" href="#-${solicitud.codSolicitud}"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
							</c:if>
												
						</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
</c:if>		
	</div>
</div>




<!-- /.col-lg-12 -->

<div id="agregar" class="modal white darken-4">
	<div class="modal-header"></div>
	<div class="modal-content">
		<form:form method="post" modelAttribute="solicitudForm"
			action="/sgp/solicitudes/add" id="codSolicitud" autocomplete="off" accept-charset="UTF-8">

			<div class="row">
				<div class="input-field col s6">
					<form:input path="titulo" class="form-control" placeholder="Titulo"
						type="text" id="titulo" value="${solicitud.titulo }" />

				</div>
				<div class="input-field col s6">
					<form:input path="estado" class="form-control"
						placeholder="Estado" type="text" id="estado"
						value="${solicitud.estado}" />
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12">
					<form:select path="idUsuario" class="form-control" required="true">
						<option value="" disabled selected>Selecione el usuario</option>
						<c:forEach items="${usuarios}" var="p">
							<c:choose>
								<c:when test="${solicitud.usuario.idUsuario == p.idUsuario}">
									<form:option value="${p.idUsuario }" label="${p.username}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${p.idUsuario }" label="${p.username}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12">
					<form:input path="justificacion" class="form-control" placeholder="Justificacion"
						type="text" id="justificacion" value="${solicitud.justificacion }" />
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12">
					<form:input path="objeto" class="form-control" placeholder="Objeto"
						type="text" id="objeto" value="${solicitud.objeto }" />
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<form:input path="correo" class="form-control" placeholder="Correo"
						type="text" id="correo" value="${solicitud.correo }" />
				</div>
			</div>
			<form:hidden path="codSolicitud" value="${solicitud.idSolicitud}" />

			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>




<c:forEach items="${solicitudes}" var="solicitud">
<div id="-${solicitud.codSolicitud}" class="modal white darken-4">
	<div class="modal-header"></div>
	<div class="modal-content">
		<form:form method="post" modelAttribute="solicitudForm"
			action="/sgp/revisiones/add" id="registro" autocomplete="off" accept-charset="UTF-8">

			<div class="row">
				
					<form:hidden path="titulo"  value="${solicitud.titulo}" />

				<div class="input-field col s6">
					<form:input path="estado" class="form-control"
						placeholder="Estado" type="text" id="estado"
						value="true" />
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12">
					<form:select path="idUsuario" class="form-control" required="true">
						<option value="" disabled selected>Selecione un usuario</option>
						<c:forEach items="${usuarios}" var="p">
							<c:choose>
								<c:when test="${solicitud.usuario.idUsuario == p.idUsuario}">
									<form:option value="${p.idUsuario }" label="${p.username}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${p.idUsuario }" label="${p.username}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
			</div>

			
					<form:hidden path="justificacion"  value="${solicitud.justificacion }" />
				
					<form:hidden   path="objeto"  value="${solicitud.objeto }" />
				
					<form:hidden path="correo"  value="${solicitud.correo }" />
			
			<form:hidden path="codSolicitud" value="${solicitud.codSolicitud}" />

			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>
</c:forEach>